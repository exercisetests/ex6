#include "MathUtils.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

int test1MathUtils()
{

	try
	{
		// Tests Ex6 part 2b - math utils

		cout <<
			"***********************\n" <<
			"Test 1 - Math Utils \n" <<
			"***********************\n" << endl;

		cout <<
			"Calling MathUtils::CalHexagonArea(1)... \n" << endl;

		std::string expected = "2.598076";
		std::string got = std::to_string(MathUtils::CalHexagonArea(1));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6401");
			std::cout << " \n" << endl;
			return 6401;
		}

		cout <<
			"\nCalling MathUtils::CalHexagonArea(36)... \n" << endl;

		expected = "3367.106770";
		got = std::to_string(MathUtils::CalHexagonArea(36));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6401");
			std::cout << " \n" << endl;
			return 6401;
		}

		cout <<
			"\nCalling MathUtils::CalHexagonArea(15)... \n" << endl;

		expected = "584.567148";
		got = std::to_string(MathUtils::CalHexagonArea(15));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6401");
			std::cout << " \n" << endl;
			return 6401;
		}

		cout <<
			"\nCalling MathUtils::CalHexagonArea(1789)... \n" << endl;

		expected = "8315197.474037";
		got = std::to_string(MathUtils::CalHexagonArea(1789));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6401");
			std::cout << " \n" << endl;
			return 6401;
		}

		cout <<
			"\nCalling MathUtils::CalPentagonArea(1)... \n" << endl;

		expected = "1.720477";
		got = std::to_string(MathUtils::CalPentagonArea(1));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6402");
			std::cout << " \n" << endl;
			return 6402;
		}

		cout <<
			"\nCalling MathUtils::CalPentagonArea(12)... \n" << endl;

		expected = "247.748746";
		got = std::to_string(MathUtils::CalPentagonArea(12));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6402");
			std::cout << " \n" << endl;
			return 6402;
		}

		cout <<
			"\nCalling MathUtils::CalPentagonArea(119)... \n" << endl;

		expected = "24363.680470";
		got = std::to_string(MathUtils::CalPentagonArea(119));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6402");
			std::cout << " \n" << endl;
			return 6402;
		}

		cout <<
			"\nCalling MathUtils::CalPentagonArea(8200)... \n" << endl;

		expected = "115684900.415602";
		got = std::to_string(MathUtils::CalPentagonArea(8200));
		if (got == expected)
		{
			cout << "\033[1;32mTest Passed\033[0m\n \n" << endl;
		}
		else
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			cout << "Expected: " << expected << endl;
			cout << "Got     : " << got << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6402");
			std::cout << " \n" << endl;
			return 6402;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;


}


int main()
{
	std::cout <<
		"#############################\n" <<
		"Exercise 6 - Shapes\n" <<
		"Part 2b - Math Utils\n" <<
		"#############################\n" << std::endl;

	int testResult = test1MathUtils();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex6 Part 2A Tests Passed ******\033[0m\n \n" : "\033[1;31mEx6 Part 2A Tests Failed\033[0m\n \n") << endl;

	return testResult;
}