#include "Shape.h"
#include "Circle.h"
#include "ShapeException.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

int test2Circle()
{

	try
	{
		// Tests Ex6 part 2a - shape execptions

		cout <<
			"*********************\n" <<
			"Test 2 - Circle \n" <<
			"*********************\n" << endl;

		cout <<
			"Trying to initialize a new Circle c(\"r\", \"c\", 2) ... \n" << endl;

		Circle c("r", "par", 2);
		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		try
		{
			cout <<
				"\nTrying to initialize a new Circle p0(\"r\", \"P0\", -2) ... \n" << endl;

			Circle p0("r", "P0", -2);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6301");
			std::cout << " \n" << endl;
			return 6301;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6302");
			std::cout << " \n" << endl;
			return 6302;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6303");
			std::cout << " \n" << endl;
			return 6303;
		}

		try
		{
			cout <<
				"\nTrying to set radius p1.setRad(-20) ... \n" << endl;

			c.setRad(-20);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6301");
			std::cout << " \n" << endl;
			return 6301;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6302");
			std::cout << " \n" << endl;
			return 6302;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6303");
			std::cout << " \n" << endl;
			return 6303;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"#############################\n" <<
		"Exercise 6 - Error Handling\n" <<
		"Part 2a - Shape exceptions\n" <<
		"#############################\n" << std::endl;

	int testResult = test2Circle();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex6 Part 2A Tests Passed ******\033[0m\n \n" : "\033[1;31mEx6 Part 2A Tests Failed\033[0m\n \n") << endl;

	return testResult;
}