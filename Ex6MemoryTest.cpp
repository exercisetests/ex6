#include "Shape.h"
#include "Circle.h"
#include "Parallelogram.h"
#include "Rectangle.h"
#include "Quadrilateral.h"
#include "Pentagon.h"
#include "Hexagon.h"

#include <vector>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

bool test4Memory()
{
	bool result = false;

	try
	{
		// Tests Ex6 part 2 - memory test

		cout <<
			"****************\n" <<
			"Memory Tests \n" <<
			"****************\n" << endl;

		cout <<
		"\nInitializing Shape objects  ... \n" << endl;

		std::vector<Shape*> shapes;
		shapes.push_back(new Parallelogram ("Pa", "r", 2, 3.5, 99.9, 80.1));
		shapes.push_back(new Circle("C1", "r",100));
		shapes.push_back(new Quadrilateral("Q", "r", 100, 250));
		shapes.push_back(new rectangle("R", "r", 120, 200));
		shapes.push_back(new Circle("C2", "r", 200));
		shapes.push_back(new Hexagon("H", "r", 30));
		shapes.push_back(new Pentagon("Pe", "r", 20));

		cout <<
			"\nDeleting Shape objects ... \n" << endl;

		for (unsigned int i = 0; i < shapes.size(); i++)
		{
			delete shapes[i];
		}
		shapes.clear();
	}
	catch (...)
	{
		std::cerr << "Test crashed" << endl;
		std::cout << "FAILED: The program crashed, check the following things:\n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return false;
	}

	cout << "\n########## Memory - TEST Complete Without Failures!!! ##########\n\n";

	return true;

}


int main()
{
	std::cout <<
		"#############################\n" <<
		"Exercise 6 - Shapes\n" <<
		"Part 2 - Memory Tests\n" <<
		"#############################\n" << std::endl;

	bool testResult = test4Memory();

	if (testResult)
	{
		std::cout << "\n########## Ex6 Memory Tests Passed!!! ##########" << "\n\n";
	}
	else
	{
		std::cout << "\n########## TEST Failed :( ##########\n";
	}
	return testResult ? 0 : 1;
}