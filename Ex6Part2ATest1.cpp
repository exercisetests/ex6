#include "Shape.h"
#include "Parallelogram.h"
#include "ShapeException.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;

int test1Parallelogram()
{

	try
	{
		// Tests Ex6 part 2a - shape execptions - parrallelogram

		cout <<
			"**************************\n" <<
			"Test 1 - Parallelogram \n" <<
			"**************************\n" << endl;

		cout <<
			"Trying to initialize a new parallelogram par(\"r\", \"par\", 2, 3.5, 120, 60) ... \n" << endl;

		Parallelogram par("r", "par", 2, 3.5, 120, 60);
		cout << "\033[1;32mOK\033[0m\n \n" << endl;

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p0(\"r\", \"P0\", -2, 3.5, 119.7, 60.3) ... \n" << endl;

			Parallelogram p0("r", "P0", -2, 3.5, 119.7, 60.3);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6201");
			std::cout << " \n" << endl;
			return 6201;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6202");
			std::cout << " \n" << endl;
			return 6202;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6203");
			std::cout << " \n" << endl;
			return 6203;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p1(\"r\", \"P1\", 2, -3.5, 119.7, 60.3) ... \n" << endl;

			Parallelogram p1("r", "P1", 2, -3.5, 119.7, 60.3);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6204");
			std::cout << " \n" << endl;
			return 6204;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6205");
			std::cout << " \n" << endl;
			return 6205;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6206");
			std::cout << " \n" << endl;
			return 6206;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p2(\"r\", \"P2\", 2, 3.5, -120, 60) ... \n" << endl;

			Parallelogram p2("r", "P2", 2, 3.5, -120, 60);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6207");
			std::cout << " \n" << endl;
			return 6207;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6208");
			std::cout << " \n" << endl;
			return 6208;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6209");
			std::cout << " \n" << endl;
			return 6209;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p3(\"r\", \"P3\", 2, 3.5, 120, -60) ... \n" << endl;

			Parallelogram p3("r", "P3", 2, 3.5, 120, -60);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6207");
			std::cout << " \n" << endl;
			return 6207;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6208");
			std::cout << " \n" << endl;
			return 6208;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6209");
			std::cout << " \n" << endl;
			return 6209;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p4(\"r\", \"P4\", 2, 3.5, 190, 60) ... \n" << endl;

			Parallelogram p4("r", "P4", 2, 3.5, 190, 60);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6210");
			std::cout << " \n" << endl;
			return 6210;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6211");
			std::cout << " \n" << endl;
			return 6211;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6212");
			std::cout << " \n" << endl;
			return 6212;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p5(\"r\", \"P5\", 2, 3.5, 120, 181) ... \n" << endl;

			Parallelogram p5("r", "P5", 2, 3.5, 120, 181);

			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6210");
			std::cout << " \n" << endl;
			return 6210;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6211");
			std::cout << " \n" << endl;
			return 6211;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6212");
			std::cout << " \n" << endl;
			return 6212;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p6(\"r\", \"P6\", 2, 3.5, 180, 0) ... \n" << endl;

			Parallelogram p6("r", "P6", 2, 3.5, 0, 180);

			cout << "\033[1;32mOK\033[0m\n \n" << endl;

			cout <<
				"\nTrying to initialize a new parallelogram p7(\"r\", \"P7\", 2, 3.5, 0, 180) ... \n" << endl;

			Parallelogram p7("r", "P7", 2, 3.5, 0, 180);

			cout << "\033[1;32mOK\033[0m\n \n" << endl;

		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6213");
			std::cout << " \n" << endl;
			return 6213;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6214");
			std::cout << " \n" << endl;
			return 6214;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6215");
			std::cout << " \n" << endl;
			return 6215;
		}

		try
		{
			cout <<
				"\nTrying to initialize a new parallelogram p8(\"r\", \"P8\", 2, 3.5, 81, 100) ... \n" << endl;

			Parallelogram p8("r", "P8", 2, 3.5, 81, 100);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6216");
			std::cout << " \n" << endl;
			return 6216;
		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6217");
			std::cout << " \n" << endl;
			return 6217;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6218");
			std::cout << " \n" << endl;
			return 6218;
		}

		try
		{
			cout <<
				"\nInitializing a new parallelogram p(\"r\", \"P\", 2, 3.5, 99.9, 80.1) ... \n" << endl;
			Parallelogram p("r", "P", 2, 3.5, 99.9, 80.1);

			cout << "\033[1;32mOK\033[0m\n \n" << endl;

			cout <<
				"\nTrying to set angle p.setAngle(120 , 60) ... \n" << endl;

			p.setAngle(120, 60);

			cout << "\033[1;32mOK\033[0m\n \n" << endl;

		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6213");
			std::cout << " \n" << endl;
			return 6213;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6214");
			std::cout << " \n" << endl;
			return 6214;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6215");
			std::cout << " \n" << endl;
			return 6215;
		}

		try
		{
			cout <<
				"\nInitializing a new parallelogram p(\"r\", \"P\", 2, 3.5, 99.9, 80.1) ... \n" << endl;
			Parallelogram p("r", "P", 2, 3.5, 99.9, 80.1);

			cout << "\033[1;32mOK\033[0m\n \n" << endl;

			cout <<
				"\nTrying to set angle p.setAngle(120 , 61) ... \n" << endl;


			p.setAngle(120, 61);
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6216");
			std::cout << " \n" << endl;
			return 6216;

		}
		catch (const ShapeException& e)
		{
			cout << "\033[1;32mOK - Got ShapeException\033[0m\n \n" << endl;
		}
		catch (const std::exception& e)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			cout << "\033[1;31m" << "Got <" << typeid(e).name() << ">" "\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6217");
			std::cout << " \n" << endl;
			return 6217;
		}
		catch (...)
		{
			cout << "\033[1;31mTest Failed\033[0m\n \n" << endl;
			std::cout << " \n" << endl;
			system("./printMessage 6218");
			std::cout << " \n" << endl;
			return 6218;
		}

	}
	catch (...)
	{
		std::cerr << "\033[1;31mTest crashed\033[0m\n \n" << endl;
		std::cerr << "\033[1;31mFAILED: The program crashed, check the following things:\n\033[0m\n \n" <<
			"1. Did you delete a pointer twice?\n2. Did you access index out of bounds?\n" <<
			"3. Did you remember to initialize array before accessing it?";
		return 2;
	}

	return 0;

}


int main()
{
	std::cout <<
		"#############################\n" <<
		"Exercise 6 - Error Handling\n" <<
		"Part 2a - Shape exceptions\n" <<
		"#############################\n" << std::endl;

	int testResult = test1Parallelogram();

	cout << (testResult == 0 ? "\033[1;32m \n****** Ex6 Part 2A Tests Passed ******\033[0m\n \n" : "\033[1;31mEx6 Part 2A Tests Failed\033[0m\n \n") << endl;

	return testResult;
}